﻿#NAV
#Gat data from Nav for table creation
function GetODBCDatafortable{
    param(
    [System.Data.Odbc.OdbcConnection]$conn=$(throw 'conn is required.'),
    [string]$query=$(throw 'query is required.')
    )
    $cmd = $conn.CreateCommand()
    $cmd.CommandText = $query
    $reader = $cmd.ExecuteReader()
    $ret = $reader.Read()
    $thash = [System.Collections.Specialized.OrderedDictionary]@{}
    for ($i = 0; $i -lt $reader.FieldCount; $i++) {
        $thash[$reader.GetName($i)] = GetSQLType -type $reader.GetDataTypeName($i)
    }
    return $thash
}

#Change NAV native types to MS SQL
function GetSQLType {
    param(
    [string]$type=$(throw 'type is required.')
    )
    switch ($type) {
       "CODE" { return "NVARCHAR(128)" }
       "STRING" { return "VARCHAR(8000)" }
       "BCD" { return "REAL" }
       "S32" { return "INT" }
       "DATE" { return "DATE" }
       "BOOL" { return "BIT" }
       default { return "VARCHAR(8000)" }
    }
}

#Gat data from Nav for table population
function GetODBCData{
    param(
    [System.Data.Odbc.OdbcConnection]$conn=$(throw 'conn is required.'),
    [string]$query=$(throw 'query is required.')
    )
    $ds = New-Object System.Data.DataSet
    $cmd = New-object System.Data.Odbc.OdbcCommand($query,$conn)
    (New-Object System.Data.Odbc.OdbcDataAdapter($cmd)).fill($ds) | out-null
    return $ds
}

#MSSQL
#delete if exists and then create MSSQL table
function CreateTableSQL {
    param(
    [System.data.sqlclient.SQLconnection]$conn=$(throw 'conn is required.'),
    [string]$name=$(throw "name is required."),
    [hashtable]$tablehash=$(throw 'tablehash is required.')
    )

    $sqlcmd = New-Object System.Data.Sqlclient.SqlCommand
    $sqlcmd.connection = $conn
    $sqlcmd.CommandTimeout = 600000
    $createtableqry = 'CREATE TABLE "{0}" (' -f $name
    foreach ($key in $tablehash.Keys) {
        $createtableqry += '"{0}" {1},' -f $key,$tablehash[$key].ToString()
    }
    $createtableqry = $createtableqry.Substring(0,$createtableqry.Length-1) + ");"
    $sqlcmd.CommandText = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{0}]') AND type in (N'U')) DROP TABLE [dbo].[{0}]" -f $name
    $rowsAffected = $sqlcmd.ExecuteNonQuery()
    $sqlcmd.CommandText = $createtableqry
    $rowsAffected = $sqlcmd.ExecuteNonQuery()
}

#populate MSSQL table with bulkcopy
function FillTableSQLBulk {
    param(
    [system.data.sqlclient.sqlconnection]$conn=$(throw 'conn is required.'),
    [string]$name=$(throw "name is required."),
    [hashtable]$tablehash=$(throw 'tablehash is required.'),
    [system.data.dataset]$ds=$(throw "ds is required")
    )
    $bulkc = New-Object System.Data.SqlClient.SqlBulkCopy -ArgumentList $conn, $([System.Data.SqlClient.SqlBulkCopyOptions]::KeepNulls -bor 0),$null
    $bulkc.EnableStreaming = $true
    $bulkc.DestinationTableName = "[dbo].[{0}]" -f $name
    $bulkc.BatchSize = 500
    foreach ($key in $tablehash.keys) {
        $bulkc.ColumnMappings.Add($key, $key)
    }
    #$bulkc.BulkCopyTimeout = 0 
    $bulkc.WriteToServer($ds.Tables[0])
}

#populate MSSQL table
function FillTableSQL {
    param(
    [system.data.sqlclient.sqlconnection]$conn=$(throw 'conn is required.'),
    [string]$name=$(throw "name is required."),
    [hashtable]$tablehash=$(throw 'tablehash is required.'),
    [system.data.dataset]$ds=$(throw "ds is required")
    )
    
    $sqlcmd = New-Object System.Data.Sqlclient.SqlCommand
    $sqlcmd.connection = $conn
    $sqlcmd.CommandTimeout = 600000

    $cnt = 0
    $dslen = $ds.Tables[0].Rows.Count

    $inputstrheader = 'INSERT INTO [dbo].[{0}] (' -f $name
    foreach ($key in $tablehash.Keys) {
        $inputstrheader += '"{0}",' -f $key
    }
    $inputstrheader = $inputstrheader.Substring(0,$inputstrheader.Length-1) + ") VALUES ";

    $inputstr = $inputstrheader
    foreach ($item in $ds.Tables[0].Rows) {
        $inputstr += '('
        foreach ($key in $tablehash.Keys) {
            if ($tablehash[$key].ToString() -eq "REAL") {
                $val = $($item."$key") -replace ',', '.'
                $inputstr += "{0}," -f $val
            } elseif ($tablehash[$key].ToString() -eq "INT") {
                $inputstr += "{0}," -f $($item."$key")
            } else {
                $val = $($item."$key") -replace "'", "''"
                $inputstr += "'{0}'," -f $val
            }
        }
        $inputstr = $inputstr.Substring(0,$inputstr.Length-1) + "),";
        $script:cnt++

        if (($script:cnt % 500 -eq 0) -or ($script:cnt -ge $dslen)) {
            $inputstr = $inputstr.Substring(0,$inputstr.Length-1) + ";";

            $sqlcmd.CommandText = $inputstr
            $rowsAffected = $sqlcmd.ExecuteNonQuery()

            $inputstr = $inputstrheader
        }
    }
}

#main
function CopyNavDatabaseToMSSQL {
    param(
    [string]$navdns=$(throw 'dns is required.'),
    [string]$navquery=$(throw 'navquery is required.'),
    [string]$tablename=$(throw 'tablename is required.'),
    [string]$mssqlConnectionString=$(throw 'connectionstring is required')
    )

    $navconn = New-Object System.Data.Odbc.OdbcConnection
    $navconn.ConnectionString = "DSN=$dns;"
    $navconn.open()

    $tablehash = [System.Collections.Specialized.OrderedDictionary]@{}
    $tablehash = GetODBCDatafortable -conn $navconn -query $navquery

    $mssqlconn = New-Object System.Data.Sqlclient.SqlConnection
    $mssqlconn.ConnectionString = $mssqlConnectionString;
    $mssqlconn.open()
    
    CreateTableSQL -conn $mssqlconn -name $tablename -tablehash $tablehash

    $ds = New-Object System.Data.DataSet
    $ds = GetODBCData -conn $navconn -query $navquery

    #FillTableSQL -conn $mssqlconn -name $tablename -tablehash $tablehash -ds $ds
    FillTableSQLBulk -conn $mssqlconn -name $tablename -tablehash $tablehash -ds $ds
    
    $navconn.close()
    $mssqlconn.close()
}

#Run script for all in file dbdata.json
function work {
param (
[string]$datafile=$(throw 'datafile is required')
)
    $data = Get-Content -encoding UTF8 $datafile | Out-String | ConvertFrom-Json

    foreach ($block in $data) {
        $dns = $block.dns
        $mssqlConnectionString = $block.mssqlConnectionString
        $qry = $block.qry
        $tname = $block.tname
        CopyNavDatabaseToMSSQL -navdns $dns -navquery $qry -tablename $tname -mssqlConnectionString $mssqlConnectionString
    }
}

work -datafile ".\data.json" 1>$null